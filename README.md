# birdnest

## What's 'birdnest'?

`birdnest` is a `pytest-bdd` template for GitLab. It is a starting point for your `pytest-bdd` project. Create a project
based on it will be able to run gitlab-ci pipeline with parameters(marker) to verify features and scenarios. 

## Why should I build it?

I have been running teams in different phases. BDD test is a blind spot for development teams IMO. Some of my team 
members have been trying to build a test framework for my teams. They might get progress by small set of pytest or 
something else. `BDD` is a good way to communicate with product owners and developers. It is a good way to build 
different set of tests for different teams. I found `pytest-bdd` is the best fit for my teams, I would share this
with rest of the community.

Meanwhile, the gitlab is another great tool to leverage. Regardless you are using gitlab cloud or on-promise, you can 
build your own CI/CD pipeline for BDD testing. It is a great way to run your tests in different environments.

## Getting started

TBD


## Installation


## Usage


## Support

As a starting point, please use the gitlab issue to raise your question.

## Roadmap

0.0.1 - Draft release

- [ ] Create a template for pytest-bdd, follow a lot of pytest-bdd best practise
- [ ] Gitlab CI pipeline to run `pytest-bdd`
- [ ] Basic convention to keep feature files and step definition files
- [ ] Use gitlab artifacts keep test data

## Contributing

TBD

## Authors and acknowledgment

Thanks all of my formal team members to share me their ideas and code.

Great work from [pytest-bdd](https://pytest.org) team.

## License
Distributed under the terms of the MIT license, `birdnest` is free and open source software.



## Project status

The project is still in it early stage, it is not ready for production yet. Use it with caution.