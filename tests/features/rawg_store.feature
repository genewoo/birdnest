@rawg_store
Feature: RAWG features Store

  Scenario: List Store
    Given I have a client
    When client list stores
    Then stores count should have more than 1

  Scenario: Get Store
    Given I have a client
    When client get stores which id is 1
    Then the result should have id
    Then the result should have domain
    Then the result should have slug
    Then the result should have description
    Then the result should have name
    Then the result should have image_background
    Then the result should have games_count
    Then the name should be Steam
    Then the domain should be store.steampowered.com
    Then the slug should be steam
    Then the id value should be 1
    Then the games_count value should more than 1

  Scenario Outline: List Games with Filter
    Given I have a client
    When client list games with filter <filter> and <value>
    Then games count should have more than <count>

    Examples:
      | filter | value | count |
      | stores | 1     | 1     |
      | stores | 2     | 1     |
      | stores | 1,2   | 1     |


  Scenario Outline: Get Game
    Given I have a client
    When client get games which id is <idx>
    Then the result should have id
    Then the result should have name
    Then the result should have slug
    Then the result should have description
    Then the result should have rating
    Then the result should have esrb_rating
    Then the result should have publishers
    Then the ratings_count value should more than <rating_count>
    Examples:
      | idx  | rating_count |
      | 3498 | 1000         |
      | 3328 | 1000         |
