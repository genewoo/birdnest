@rawg_game
Feature: RAWG features Game

  Scenario Outline: List Games with Filter
    Given I have a client
    When client list games with filter <filter> and <value>
    Then games count should have more than <count>

    Examples:
      | filter | value | count |
      | stores | 1     | 1     |
      | stores | 2     | 1     |
      | stores | 1,2   | 1     |


  Scenario Outline: Get Game
    Given I have a client
    When client get games which id is <idx>
    Then the result should have id
    Then the result should have name
    Then the result should have slug
    Then the result should have description
    Then the result should have rating
    Then the result should have esrb_rating
    Then the result should have publishers
    Then the ratings_count value should more than <rating_count>
    Examples:
      | idx  | rating_count |
      | 3498 | 1000         |
      | 3328 | 1000         |
