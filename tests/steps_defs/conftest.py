import os

from pytest_bdd import given, when, then, parsers

from pytest import fixture

from client import Client


@fixture
def key():
    return os.getenv("API_KEY")


@given("I have a client", target_fixture="client")
def step_impl(key):
    return Client(key, 'https://api.rawg.io/api/')


@when(parsers.re(r'client list (?P<resources>\w+)'),
      target_fixture="result")
def step_impl(client, resources):
    return client.send(resources, 'get', {'page_size': 10})


@then(parsers.re(r'(?P<resources>\w+) count should have more than (?P<count>\d+)'))
def step_impl(result, count):
    assert (result.json()['count'] > int(count))


@when(parsers.re(r'client get (?P<resources>\w+) which id is (?P<xid>\d+)'), target_fixture="result")
def step_impl(client, resources, xid):
    return client.send(f'{resources}/{xid}', 'get')


@then(parsers.re(r"the result should have (?P<key>\w+)"))
def step_impl(result, key):
    assert (result.json()[key] is not None)


@then(parsers.re(r"the (?P<key>\w+) should be (?P<value>.*)"))
def step_impl(result, key, value):
    assert (result.json()[key] == value)


@then(parsers.re(r"the (?P<key>\w+) value should (?P<compare>(be|more than|less than)) (?P<value>.*)"))
def step_impl(result, compare, key, value):
    if compare == "be":
        assert (result.json()[key] == int(value))
    elif compare == "more than":
        assert (result.json()[key] > int(value))
    elif compare == "less than":
        assert (result.json()[key] < int(value))


@when(parsers.re(r'client list (?P<resources>\w+) with filter (?P<fitler>\w+) and (?P<value>.*$)'), target_fixture="result")
def step_impl(client, resources, fitler, value):
    return client.send(resources, 'get', {'page_size': 10, fitler: value})
