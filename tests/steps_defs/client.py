import requests


# HTTPClient is an object with a property of api key

class Client(object):
    def __init__(self, api_key, base_url):
        print(api_key)
        print(base_url)
        self.api_key = api_key
        self.base_url = base_url

    # send method to send request, the params will include api key as "key"
    def send(self, uri, method, params=None, data=None, json=None, headers=None, files=None, **kwargs):
        params = params or {}
        params['key'] = self.api_key

        # join uri with base_url
        url = self.base_url + uri

        if method.lower() == 'get':
            return requests.get(url, params=params, headers=headers, **kwargs)
        elif method.lower() == 'post':
            return requests.post(url, data=data, json=json, headers=headers, files=files, **kwargs)
        elif method.lower() == 'put':
            return requests.put(url, data=data, json=json, headers=headers, files=files, **kwargs)
        elif method.lower() == 'delete':
            return requests.delete(url, params=params, headers=headers, **kwargs)
        else:
            raise Exception('Not support method: {}'.format(method))

