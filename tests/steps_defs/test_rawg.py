from pytest_bdd import scenarios

scenarios('../features/rawg_store.feature')

scenarios('../features/rawg_game.feature')
